<?php


/**
 * Color-Plugin
 *
 * @category  NEOM
 * @package   Shopware\Plugins\Frontend\NeomEmotionElement
 */
class Shopware_Plugins_Frontend_NeomEmotionElement_Bootstrap extends Shopware_Components_Plugin_Bootstrap{

  public    $component;
  private   $xtype = 'neom-emotion-element';

  /**
   * Returns all necessary information about the plugin.
   *
   * @return array
   */
  public function getInfo(){
    return array(
      'version' => $this->getVersion(),
      'label' => $this->getLabel(),
      'supplier' => 'NEOM GmbH.',
      'description' => 'Mehrere Möglichkeiten einzelne Kacheln zu gestalten',
      'support' => 'Shopware Forum',
      'autor' => 'NEOM GmbH',
      'license' => 'Proprietär',
      'copyright' => '&copy; 2014 NEOM GmbH',
      'link' => 'http://www.neom-agentur.de'
    );
  }

  /**
   * Returns the name of the plugin.
   *
   * @return string
   */
  public function getLabel(){
    return 'Neom Emotion Element';
  }

  /**
   * Returns the version of the plugin.
   *
   * @return string
   */
  public function getVersion(){
      return "1.0.0";
  }


 /**
 * The install function of the plugin.
 *
 * @return bool
 */
  public function install(){
      // Save the new component
      $this->component = $this->createComponent();

      // Create the necessary fields for the component
      $this->createComponentFields();

      // Register all necessary events to handle the data.
      $this->registerEvents();

      // Don't forget to return true ;)
      return true;
  }



  /**
   * The uninstall function of the plugin.
   *
   * @return bool
   */
  public function uninstall(){

        $sql = "SELECT
                      id
                  FROM
                      s_library_component
                  WHERE
                      x_type = '".$this->xtype."'
               ";
        $id = Shopware()->Db()->fetchOne($sql);
        $sql = "DELETE FROM
                  s_emotion_element
                WHERE componentID = '" . $id . "'
               ";
        $query = Shopware()->Db()->query($sql);
        $sql = "DELETE FROM
                  s_library_component
                WHERE id = '" . $id . "'
               ";
        $query = Shopware()->Db()->query($sql);
        $sql = "DELETE FROM
                  s_library_component_field
                WHERE componentID = '" . $id . "'
               ";
        $query = Shopware()->Db()->query($sql);

      return true;
  }

  /**
   * Create and return the new emotion component with custom xtype.
   *
   * @return \Shopware\Models\Emotion\Library\Component
   */
  public function createComponent()
  {
      return $this->createEmotionComponent(array(
          'name' => 'Neom Emotion Element',
          'xtype' => $this->xtype,
          'template' => 'neom_emotion_element',
          'cls' => $this->xtype,
          'description' => 'Neom Emotion Element'
        ));
  }

  /**
   * Create a hidden input field for the emotion component configuration.
   * The hidden field will be used to store the data of the custom ExtJS Component.
   * Data will be stored as a JSON string.
   */
  public function createComponentFields(){

    $this->component->createTextField(array(
      'name' => 'title',
      'fieldLabel' => 'Title',
      'supportText' => 'Title der Kachel',
      'helpTitle' => 'Title',
      'helpText' => 'Title der Kachel',
      'defaultValue' => '',
      'allowBlank' => true
    ));

    $this->component->createTextField(array(
      'name' => 'link',
      'fieldLabel' => 'Link',
      'supportText' => 'Link',
      'helpTitle' => 'Link',
      'helpText' => 'Link',
      'defaultValue' => '',
      'allowBlank' => true
    ));

    $this->component->createTextAreaField(array(
      'name' => 'text',
      'defaultValue' => '',
      'fieldLabel' => 'Text',
      'supportText' => 'Text',
      'helpTitle' => 'Text',
      'helpText' => 'Text',
      'allowBlank' => true
    ));

    $this->component->createHiddenField(array(
        'name' => 'neom_emotion_store',
        'allowBlank' => true
    ));

    $this->component->createCheckboxField(array(
      'name' => 'video',
      'fieldLabel' => 'Videolink?',
      'supportText' => 'Video',
      'helpTitle' => 'Videolink?',
      'helpText' => 'Handelt es sich bei dem Link um ein Video?',
      'allowBlank' => true
    ));
    $this->component->createCheckboxField(array(
      'name' => 'animieren',
      'fieldLabel' => 'Text animieren?',
      'supportText' => 'Text animieren',
      'helpTitle' => 'Text animieren?',
      'helpText' => 'Text animieren',
      'allowBlank' => true
    ));

    $this->component->createCheckboxField(array(
      'name' => 'autoheight',
      'fieldLabel' => 'Automatische Höhe',
      'supportText' => 'Höhe abhängig vom Inhalt anpassen',
      'allowBlank' => true
    ));

  }

  /**
   * Register on the emotion filter event to handle
   * the saved data before passing it to the template.
   */
  public function registerEvents(){
      $this->subscribeEvent(
          'Shopware_Controllers_Widgets_Emotion_AddElement',
          'onEmotionAddElement'
      );
  }


  /**
   * Event handler for the emotion filter event.
   * It decodes the stored JSON string and passes the values to the emotion data.
   *
   * @param Enlight_Event_EventArgs $arguments
   * @return mixed
   */
  public function onEmotionAddElement($arguments){
    $data = $arguments->getReturn();
    $files = array();
    if (isset($data['neom_emotion_store']) &&
        !empty($data['neom_emotion_store'])) {
        $files =  json_decode($data['neom_emotion_store'], true);
    }

    $data['image'] = $files;

    return $data;
  }


}
