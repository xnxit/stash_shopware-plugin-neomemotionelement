<div class="neom-emotion-element"{if $Data.image.0.path} style="background-image:url(/{$Data.image.0.path});"{/if}>
	{if $Data.link}
		<a href="{$Data.link}"  data-title="{$Data.title}" {if $Data.video}data-toggle="lightbox"{/if}>
	{/if}
	{if $Data.video}
		<div class="video-icon"></div>
	{/if}

		{if $Data.text}
			<div class="text hidden-xs">
				<div class="inner">
					<h3>{$Data.title}</h3>
					{$Data.text}
				</div>
			</div>
		{/if}


		<div class="title">
			{$Data.title}
		</div>

	{if $Data.link}
		</a>
	{/if}
</div>
